"""image_inliner
Convert images to embeddable HTML
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""

import base64


def open_image(image_path):
    """Read image file to bytes"""
    with open(image_path, "rb") as ifile:
        return ifile.read()


def image_to_b64(image_bytes):
    """Encode image bytes to base64"""
    return base64.b64encode(image_bytes)


def b64_bytes_decode(b64_bytes):
    """Then decode b64 to a string useful to html"""
    return b64_bytes.decode()


def render_inline_html(image_type, b64_image, alt_text, *argv):
    img_tag = "<img src='data:image/{};base64,{}' alt='{}'".format(
        image_type, b64_image, alt_text
    )
    img_tag = "{}>".format(img_tag)
    return img_tag


def render_inline_html_size(image_type, b64_image, alt_text, width, height):
    img_tag = "<img src='data:image/{};base64,{}' alt='{}', width='{}', height='{}'>".format(
        image_type, b64_image, alt_text, width, height
    )
    return img_tag


def render_css_image(image_type, b64_image):
    css_image = """<style type='text/css'>
                div.cssimage {
                    width:{}px;
                    height:{}px;
                    background-image: url('data:image/{};base64,{}');
                    }</style>""".format(
        image_type, b64_image
    )
    return css_image


def render_css_image_size(image_type, b64_image, width, height):
    css_image = """<style type='text/css'>
                div.cssimage {
                    width:{}px;
                    height:{}px;
                    background-image: url('data:image/{};base64,{}');
                    }</style>""".format(
        str(width), str(height), image_type, b64_image
    )
    return css_image


def img_to_html(image_path):
    alt_text, image_type = image_path.rsplit(".", 1)
    image_bytes = open_image(image_path)
    b64_bytes = image_to_b64(image_bytes)
    b64_image = b64_bytes_decode(b64_bytes)
    img_tag = render_inline_html(image_type, b64_image, alt_text)
    return img_tag


def img_to_html_size(image_path, size):
    alt_text, image_type = image_path.rsplit(".", 1)
    width, height = size.split("x", 1)
    image_bytes = open_image(image_path)
    b64_bytes = image_to_b64(image_bytes)
    b64_image = b64_bytes_decode(b64_bytes)
    img_tag = render_inline_html_size(image_type, b64_image, alt_text, width, height)
    return img_tag


if __name__ == "__main__":
    """image2inline.py
    """
    import sys
    try:
        img_inline = img_to_html_size(image_path=sys.argv[1], size=sys.argv[2])
    except:
        img_inline = img_to_html(image_path=sys.argv[1])
    finally:
        print(img_inline)