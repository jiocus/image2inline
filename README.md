
![alt image2inline BMW M tribute logo](image2inline_less.png)
`image2inline.py`

Very brief utility script to generate HTML <img> tag with the image inline, embedded with the markup.

If your images, like mine, also have *filename+file ending* we can automatically;
- Set `alt` attribute from filename,
- Set `image/<type>` from file ending.

```sh
python image2inline.py image.png
<img src='data:image/png;base64,iVBORw0KGgoAAAANS [..] lFTkSuQmCC' alt='image'>
```

Set a resolution for the resulting markup,
```sh
python image2inline.py image.png 100x100
<img src='data:image/png;base64,iVBORw0KGgoAAAANS [..] lFTkSuQmCC' alt='image', width='100', height='100'>
```


```html
<img src='data:image/png;base64,iVBORw0KGgoAAAANS [..] lFTkSuQmCC' alt='image2inline_less'>
```
